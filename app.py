from flask import Flask
from flask import request
from flask import render_template
from flask import session
from flask import jsonify
import MySQLdb
import calendar
import os
import datetime

database_host = "localhost" #change these lines 
database_user = "root"
database_password = "password"
database_db = "amen"

def generate_calendar(year, month):
	result = {}
	date_in_string = calendar.month(year, month)
	date_in_list = date_in_string.split("\n")
	year_month = date_in_list[0]
	result["year_month"] = year_month[3:]
	date_in_list = date_in_list[2:len(date_in_list)-1]
	for i in range(len(date_in_list)):
		date_in_list[i] = date_in_list[i].split()
	result["sixth_line"] = False
	if len(date_in_list) == 6:
		result["sixth_line"] = True
	result["first_row"] = date_in_list[0]
	result["last_row"] = date_in_list[len(date_in_list)-1]
	result["first_row_padding"] = 7 - len(result["first_row"])
	result["last_row_padding"] = 7 - len(result["last_row"])
	result["middle_row"] = date_in_list[1:len(date_in_list)-1]
	return result

app = Flask(__name__)
@app.route("/")
def main():
	now = datetime.datetime.now()
	year = now.year
	month = now.month
	session["year"] = year
	session["month"] = month
	result = generate_calendar(year, month)
	return render_template("index.html", first_row_padding = result["first_row_padding"], first_row = result["first_row"] , middle_rows = result["middle_row"], last_row_padding = result["last_row_padding"], last_row = result["last_row"], sixth_line = result["sixth_line"], year_month = result["year_month"], year = year, month = month)

@app.route("/next_month")
def next_month():
	year = session["year"]
	month = session["month"]
	if(month>=12):
		session["year"] = year+1
		year = session["year"]
		session["month"] = (session["month"]+1)%12
		month = session["month"]
	else:
		month +=1
		session["month"] = month
	
	result = generate_calendar(year, month)
	template = render_template("next_month.html", first_row_padding = result["first_row_padding"], first_row = result["first_row"] , middle_rows = result["middle_row"], last_row_padding = result["last_row_padding"], last_row = result["last_row"], sixth_line = result["sixth_line"], year_month = result["year_month"], year = year, month = month)
	dict = {"next_month":template}
	return jsonify(dict)

@app.route("/previous_month")
def previous_month():
	year = session["year"]
	month = session["month"]-1
	if(month<=0):
		session["year"] = year-1
		year = session["year"]
		session["month"] = 12
		month = session["month"]
	else:
		session["month"] = month
	
	result = generate_calendar(year, month)
	template = render_template("next_month.html", first_row_padding = result["first_row_padding"], first_row = result["first_row"] , middle_rows = result["middle_row"], last_row_padding = result["last_row_padding"], last_row = result["last_row"], sixth_line = result["sixth_line"], year_month = result["year_month"], year = year, month = month)
	dict = {"previous_month":template}
	return jsonify(dict)


@app.route("/get_create_template", methods = ["GET", "POST"])
def get_create_template():
	if(request.method == "POST"):
		box_id = request.form["box_id"]
		template = render_template("get_create_template.html", box_id = box_id)
		return_dict = {"template":template}
		return jsonify(return_dict)

@app.route("/create_event", methods = ["GET", "POST"])
def create_event():
	if request.method == "POST":
		box_id = request.form["box_id"]
		event_name = request.form["event_name"]
		location = request.form["location"]
		start_time = int(request.form["start_time"])
		end_time = int(request.form["end_time"])
		event_description = request.form["event_description"]
		db = MySQLdb.connect("localhost","root","9958845774","amen")
		cursor = db.cursor()
		cursor.execute(""" insert into events (box_id, event_name, location, start_time, end_time, description) values("%s","%s","%s",%d,%d,"%s") """%(box_id, event_name, location, start_time, end_time, event_description))
		cursor.execute("commit")
		db.close()

@app.route("/display_event", methods = ["GET", "POST"])
def display_event():
	if request.method == "POST":
		box_id = int(request.form["box_id"])
		db = MySQLdb.connect(database_host, database_user, database_password, database_db)
		cursor = db.cursor()
		cursor.execute("""select event_id, event_name, location, start_time, end_time, description from events where box_id = %d"""%box_id)
		result = cursor.fetchall()
		db.close()
		if( not result):
			dictionary = {"template":"nothing"}
			return dictionary
		dt = {}
		data = []
		column_field =["event_id", "event", "location", "start_time", "end_time", "description"]
		for i in range(len(result)):
			for j in range(6):
				dt[column_field[j]] = result[i][j]
			data.append(dt)
		template = render_template("display_event.html", data = data)
		dictionary = {"template":template}
		return jsonify(dictionary)
@app.route("/delete_event", methods = ["GET", "POST"])
def delete_event():
	if request.method == "POST":
		event_id = int(request.form["event_id"])
		db = MySQLdb.connect(database_host, database_user, database_password, database_db)
		cursor = db.cursor()
		cursor.execute("""delete from events where event_id = %d """%event_id)
		cursor.execute("commit")
		db.close()
		return "something"
		
			
	

app.secret_key = os.urandom(24)

if __name__ == "__main__":
	app.run(debug = True)
