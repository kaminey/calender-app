create database amen;
use database amen;
create table events
(
	event_id int not null auto_increment primary key,
	box_id int not null,
	event_name varchar(40),
	location varchar(40),
	start_time int,
	end_time int,
	description varchar(500)
);
